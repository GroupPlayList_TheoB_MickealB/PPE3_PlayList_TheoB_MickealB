# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 17:18:03 2016

@author: Mickael Boutruche and Theo Bouchevreau
"""

import logging
logger = logging.getLogger('generator')
logger.setLevel(logging.DEBUG)


class Artist():
    def __init__(self, _artistName):
        """Constructor Artist
        """
        logger.info("Initialization of Artist")
        self.artistName= str(_artistName)
        
    def getArtistName(self):
        """ Return Artist ArtistName as a string()
        """
        return self.artistName
        
    def setArtistName(self,_artistName):
        """ Set Artist ArtistName using the given string()
        """
        self.artistName = str(_artistName)
    
    def __repr__(self):
        """ Return properties of the object
        """
        return "(Artist name : %s)" % (self.artistName)
            
if __name__ == "__main__":
    logger.info("Execution of module 'Artist'")
    a = Artist("David")
    logger.debug("Artist name : "+a.getArtistName())
    logger.debug("Using method '__repr__(self)' : "+str(a))
    logger.info("End of module 'Artist'")
else:
    logger.info("Loading of module Artist")