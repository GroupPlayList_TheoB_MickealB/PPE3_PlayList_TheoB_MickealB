# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 18:15:09 2016

@author: Mickael Boutruche and Theo Bouchevreau
"""

import track
import album
import artist
import audioformat
import kind
import polyphony
import logging
logger = logging.getLogger('generator')
logger.setLevel(logging.DEBUG)


class Playlist():
    def __init__(self, _duration, _name, _criterions, _trackList):
        """Constructor Playlist
        """
        logger.info("Initialization of Playlist")
        self.duration = int(_duration)
        self.name = str(_name)
        self.criterions = str(_criterions)
        self.trackList = _trackList=[]
        
    def getDuration(self):
        """ Return Playlist duration as a int()
        """
        return self.duration
        
    def setDuration(self,_duration):
        """ Set Playlist duration using the given int()
        """
        self.duration = int(_duration)
        
    def getName(self):
        """ Return Playlist Name as a string()
        """
        return self.name
        
    def setName(self,_name):
        """ Set Playlist Name using the given string()
        """
        self.name = str(_name)
    
    def getCriterions(self):
        """ Return Playlist criterions as a string()
        """
        return self.criterions
        
    def setCriterions(self,_criterions):
        """ Set Playlist criterions using the given string()
        """
        self.criterions = str(_criterions)

    def add(self):
        """ Method for adding a track into the playlist
        """
        pass
    
    def remove(self):
        """ Method for remove a track into the playlist
        """
        pass
    
    def rename(self):
        """ Method for rename the playlist
        """
        pass
    
    def order(self):
        """ Method for order the playlist
        """
        pass
    
    def export(self):
        """ Method for export the playlist in M3U or XSPF
        """
        pass
            
if __name__ == "__main__":
    logger.info("Execution of module 'Playlist'")
    myAlbum=album.Album("a title") #default album
    myArtist=artist.Artist("a artistName") #default artist
    myAudioFormat=audioformat.Audioformat(".mp3") #default audio format
    myKind=kind.Kind("rock") #default kind
    myPolyphony=polyphony.Polyphony(1,"mono") #default polyphony
    
    t1 = track.Track("track1", 120, 1, "/etc/track1",  myAlbum, myArtist, myAudioFormat, myKind, myPolyphony) #create track1
    t2 = track.Track("track2", 120, 2, "/etc/track2",  myAlbum, myArtist, myAudioFormat, myKind, myPolyphony) #create track2
    
    myList=[] #insert track here
    myList.append(t1)
    myList.append(t2)

    p = Playlist(600, "RockOnly", "--bla --bli", myList)
    
    logger.debug("Duration : "+str(p.getDuration()))
    logger.debug("Name : "+p.getName())
    logger.debug("Criterous : "+p.getCriterions())
    i=1
    for element in myList:
        logger.debug("track"+str(i))
        logger.debug(str(i)+" - Title : "+element.getTitle())
        logger.debug(str(i)+" - Duration : "+str(element.getDuration()))
        logger.debug(str(i)+" - NumTrack : "+str(element.getNumTrack()))
        logger.debug(str(i)+" - Path : "+element.getPath())
        logger.debug(str(i)+" - Album Title : "+element.getAlbum().getTitle())
        logger.debug(str(i)+" - Artist name : "+element.getArtist().getArtistName())
        logger.debug(str(i)+" - AudioFormat name : "+element.getAudioFormat().getName())
        logger.debug(str(i)+" - Kind name : "+element.getKind().getName())
        logger.debug(str(i)+" - Polyphony nbChannel : "+str(element.getPolyphony().getNbChannel()))
        logger.debug(str(i)+" - Polyphony description : "+element.getPolyphony().getDescription())
        i=i+1
    logger.info("End of module 'Playlist'")
else:
    logger.info("Loading of module 'Playlist'")