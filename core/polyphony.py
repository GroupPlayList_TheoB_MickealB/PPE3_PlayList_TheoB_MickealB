# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 17:25:10 2016

@author: etudiant
"""

import logging
logger = logging.getLogger('generator')
logger.setLevel(logging.DEBUG)


class Polyphony():
    def __init__(self, _nbChannel, _description):
        """Constructor Polyphony
        """
        logger.info("Initialization of Polyphony")
        self.nbChannel = int(_nbChannel)
        self.description = str(_description)
        
    def getNbChannel(self):
        """ Return Polyphony number channel as a int()
        """
        return self.nbChannel
        
    def setNbChannel(self,_nbChannel):
        """ Set Polyphony number channel using the given int()
        """
        self.nbChannel = int(_nbChannel)
    
    def getDescription(self):
        """ Return Polyphony description as a str()
        """
        return self.description
        
    def setDescription(self,_description):
        """ Set Polyphony description using the given str()
        """
        self.description = str(_description)
                
    def __repr__(self):
        """ Return properties of the object
        """
        return "(Channel nombre : %d, Description : %s)" % (self.nbChannel, self.description)
           
if __name__ == "__main__":
    logger.info("Execution of module 'Polyphony'")
    p=Polyphony(1,"mono")
    logger.debug("Number channel : "+str(p.getNbChannel()))
    logger.debug("Description : "+p.getDescription())
    logger.debug("Using method '__repr__(self)' : "+str(p))
    logger.info("End of module 'Polyphony'")
else:
    logger.info("Loading of module Polyphony")