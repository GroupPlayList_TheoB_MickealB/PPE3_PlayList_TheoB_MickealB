# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 16:17:26 2016

@author: Mickael Boutruche and Theo Bouchevreau
"""

import logging
logger = logging.getLogger('generator')
logger.setLevel(logging.DEBUG)


class Audioformat():
    def __init__(self, _name):
        """Constructor Audioformat
        """
        logger.info("Initialization of Audioformat")
        self.name = str(_name)
        
    def getName(self):
        """ Return Audioformat Name as a string()
        """
        return self.name
        
    def setName(self,_name):
        """ Set Audioformat Name using the given string()
        """
        self.name = str(_name)
            
    def __repr__(self):
        """ Return properties of the object
        """
        return "(Name : %s)" % (self.name)
            
if __name__ == "__main__":
    logger.info("Execution of module 'Audioformat'")
    a = Audioformat(".mp3")
    logger.debug("Format Name : "+a.getName())
    logger.debug("Using method '__repr__(self)' : "+str(a))
    logger.info("End of module 'Audioformat'")
else:
    logger.info("Loading of module 'Audioformat'")