# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 16:15:53 2016

@author: Mickael Boutruche and Theo Bouchevreau
"""

import logging
logger = logging.getLogger('generator')
logger.setLevel(logging.DEBUG)

class Album():    
    def __init__(self, _title):
        """Constructor Album
        """
        logger.info("Initialization of Album")
        self.title = str(_title)
        
    def getTitle(self):
        """ Return Album title as a string()
        """
        return self.title
        
    def setTitle(self,_title):
        """ Set Album title using the given string()
        """
        self.title = str(_title)
            
    def __repr__(self):
        """ Return properties of the object
        """
        return "(Title : %s)" % (self.title) 
                
if __name__ == "__main__":
    logger.info("Execution of module 'Album'")
    a=Album("One more time")
    logger.debug("title : "+a.getTitle())
    logger.debug("Using method '__repr__(self)' : "+ str(a))
    logger.info("End of module 'Album'")
else:
    logger.info("Loading of module 'Album'")