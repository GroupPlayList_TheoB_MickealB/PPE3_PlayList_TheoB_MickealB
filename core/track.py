# -*- coding: utf-8 -*-
"""
Created on Wed Sep 28 17:18:03 2016

@author: Mickael Boutruche and Theo Bouchevreau
"""
import album
import artist
import audioformat
import kind
import polyphony

import logging
logger = logging.getLogger('generator')
logger.setLevel(logging.DEBUG)


class Track():
    def __init__(self, _title, _duration, _numTrack, _path, _album, _artist, _audioformat, _kind, _polyphony):
        """Constructor Track
        """
        logger.info("Initialization of Track")
        self.title = str(_title)
        self.duration=int(_duration)
        self.numTrack=int(_numTrack)
        self.path=str(_path)
        self.album=_album
        self.artist=_artist
        self.audioformat=_audioformat
        self.kind=_kind
        self.polyphony=_polyphony
        
    def getTitle(self):
        """ Return Track title as a string()
        """
        return self.title
        
    def setTitle(self,_title):
        """ Set Track title using the given string()
        """
        self.title = str(_title)
            
    def getDuration(self):
        """ Return Track duration as a int()
        """
        return self.duration
    
    def setDuration(self, _duration):
        """ Set Track duration using the given int()
        """
        self.duration=int(_duration)
        
    def getNumTrack(self):
        """ Return Track numTrack as a int()
        """
        return self.numTrack
        
    def setNumTrack(self, _numTrack):
        """ Set Track numTrack using the given int()
        """
        self.numTrack=int(_numTrack)
    
    def getPath(self):
        """ Return Track path as a string()
        """
        return self.path
        
    def setPath(self, _path):
        """ Set Track path using the given str()
        """
        self.path=str(_path)
    
    def getAlbum(self):
        """ Return Album as a Album()
        """
        return self.album
        
    def setAlbum(self, _album):
        """ Set Album using the given Album()
        """
        self.album=_album
        
    def getArtist(self):
        """ Return Artist as a Artist
        """
        return self.artist
        
    def setArtist(self, _artist):
        """ Set Artist using the given Artist()
        """
        self.artist=_artist
        
    def getAudioFormat(self):
        """ Return AudioFormat as a AudioFormat()
        """
        return self.audioformat
        
    def setAudioFormat(self, _audioformat):
        """ Set AudioFormat using the given AudioFormat()
        """
        self.audioformat=_audioformat
        
    def getKind(self):
        """ Return Kind as a Kind()
        """
        return self.kind
        
    def setKind(self, _kind):
        """ Set Kind using the given Kind()
        """
        self.kind=_kind
        
    def getPolyphony(self):
        """ Return Polyphony as a Polyphony()
        """
        return self.polyphony
        
    def setPolyphony(self, _polyphony):
        """ Set Polyphony using the given Polyphony()
        """
        self.polyphony=_polyphony
        
if __name__ == "__main__":
    logger.info("Execution of module 'Track'")
    myAlbum=album.Album("a title")
    myArtist=artist.Artist("a artistName")
    myAudioFormat=audioformat.Audioformat(".mp3")
    myKind=kind.Kind("rock")
    myPolyphony=polyphony.Polyphony(1,"mono")
    
    t = Track("TrackName", 90, 1, "/etc/track", myAlbum, myArtist, myAudioFormat, myKind, myPolyphony)
    logger.debug("Title : "+t.getTitle())
    logger.debug("Duration : "+str(t.getDuration()))
    logger.debug("NumTrack : "+str(t.getNumTrack()))
    logger.debug("Path : "+t.getPath())
    logger.debug("Album Title : "+t.getAlbum().getTitle())
    logger.debug("Artist name : "+t.getArtist().getArtistName())
    logger.debug("AudioFormat name : "+t.getAudioFormat().getName())
    logger.debug("Kind name : "+t.getKind().getName())
    logger.debug("Polyphony nbChannel : "+str(t.getPolyphony().getNbChannel()))
    logger.debug("Polyphony description : "+t.getPolyphony().getDescription())
    
    logger.info("End of module 'Track'")
else:
    logger.info("Loading of module Track")