# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 19:06:43 2016

@author: Mickael Boutruche and Theo Bouchevreau
"""

import logging
logger = logging.getLogger('generator')
logger.setLevel(logging.DEBUG)


class Kind():
    def __init__(self, _name):
        """Constructor Kind
        """
        logger.info("Initialization of Kind")
        self.name= str(_name)
        
    def getName(self):
        """ Return Kind Name as a string()
        """
        return self.name
        
    def setName(self,_name):
        """ Set Kind Name using the given string()
        """
        self.name = str(_name)
            
    def __repr__(self):
        """ Return properties of the object
        """
        return "(Name : %s)" % (self.name)
            
if __name__ == "__main__":
    logger.info("Execution of module 'Kind'")
    k=Kind("Rock")
    logger.debug("Name : "+k.getName())
    logger.debug("Using method '__repr__(self)' : " +str(k))
    logger.info("End of module 'Kind'")
else:
    logger.info("Loading of module Kind")