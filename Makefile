livrable:
	python3 setup.py --command-packages=stdeb.command bdist_deb

clean-all:
	-rm -r deb_dist/
	-rm -r dist/
	-rm -r generator.egg-info/
	-rm -r generator-1.0.tar.gz
