# -*- coding: utf-8 -*-

import logging
import argparse
import psycopg2
import sys
from random import shuffle
from lxml import etree

formatter = logging.basicConfig(format='%(filename)s:%(levelname)s:%(message)s')
logger = logging.getLogger('generator')

def makem3u(nameString, fullDurationInt, playListP=[]):
    #For M3U
    fullname = str(nameString)+" " + str(fullDurationInt)+".m3u"
    file=open(fullname, "w")
    for track in playListP:
        file.write(track[2]+"\n")
    logger.info("Playlist m3u created.")
    
def makexspf(nameString, fullDurationInt, playListP=[]):
    #For XSPF
    root =etree.Element('playlist')
    trackList = etree.SubElement(root, 'trackList')
    etree.SubElement(root, 'title').text='Playlist'
    root.set("version", "1")
    root.set("xmlns","http://xspf.org/ns/0/")
      
    for mytrack in playListP:
        track = etree.SubElement(trackList, 'track')
        etree.SubElement(track, 'title').text=mytrack[0]
        etree.SubElement(track, 'duration').text=str(mytrack[1])
        etree.SubElement(track, 'creator').text=mytrack[3]
        etree.SubElement(track, 'location').text='file://'+mytrack[2]
    
    try:
        fullname = str(nameString)+" " + str(fullDurationInt)+".xspf"
        with open(fullname, 'w') as fic:
            fic.write(etree.tostring( root, pretty_print=True, xml_declaration=True, method="xml", encoding="UTF-8").decode("UTF-8"))
            logger.info("Playlist xspf created.")
    except IOError:
        logger.critical('Error while writing')
        sys.exit(0)