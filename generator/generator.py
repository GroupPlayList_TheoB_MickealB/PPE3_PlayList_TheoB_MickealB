# -*- coding: utf-8 -*-
"""
Created on Tue Sep 27 15:55:00 2016

@author: Theo Bouchevreau
@author: Mickael Boutruche
"""

import logging
import argparse
import psycopg2
import sys
from random import shuffle
from lxml import etree

try:
	from . import m3uAndXspfFile
except:
	import m3uAndXspfFile

formatter = logging.basicConfig(format='%(filename)s:%(levelname)s:%(message)s')
logger = logging.getLogger('generator')
# Copyright with --help
helpcopyright = '''
    Copyright ©2016 Théo BOUCHEVREAU and Mickaël BOUTRUCHE 
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    
    Misc If you find this software useful, please send me a postcard.
'''
# Create different arguments for the command-line
parser = argparse.ArgumentParser(
#add copyright with helpcopyright
formatter_class=argparse.RawDescriptionHelpFormatter,
    epilog=helpcopyright)
parser.add_argument("-l, --log-level", type=str, help="log level", dest="logLevel", default="INFO", choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"])
parser.add_argument("--duration",metavar=('Duration'), type=int, help="Enter duration for playlist", dest="duration")
parser.add_argument("--kind",metavar=('KindName', 'Percent'),help="Enter kind (first argument is the name and second the percentage)", nargs=2, dest="kind_name", action="append")
parser.add_argument("--artist",metavar=('ArtistName', 'Percent'), type=str, help="Enter artist (first argument is the name and second the percentage)", nargs=2, dest="artist_name", action="append")
parser.add_argument("--album",metavar=('AlbumName', 'Percent'),  type=str, help="Enter album (first argument is the name and second the percentage)", nargs=2, dest="album_name", action="append")
parser.add_argument("--title", metavar=('Title'),type=str, help="Enter title a track", dest="title", action="append")
parser.add_argument("--xspf", help="Generate the playlist in a '.xspf'", action="store_true")
parser.add_argument("--m3u", help="Generate the playlist in a '.m3u'", action="store_true")
parser.add_argument("--shuffle", help="Shuffle your Playlist", action="store_true")
parser.add_argument("--port", metavar=('Port'),type=int, help="Enter port to connect a database, default port is 62543",default="62543", dest="port")
parser.add_argument("--host", metavar=('AdressHost'),type=str, help="Enter host to connect a database, default host is bts.bts-malraux72.net",default="bts.bts-malraux72.net", dest="host")
parser.add_argument("--database", metavar=('NameDatabase'),type=str, help="Enter name of database to connect a database, default host is radio_libre",default="radio_libre", dest="database")
parser.add_argument("-u", metavar=('userDatabase'),type=str, help="Enter user to connect a database, default host is user by default",default="t.bouchevreau", dest="user")
parser.add_argument("-p", metavar=('passwordDatabase'),type=str, help="Enter password to connect a database, default host is user by default",default="P@ssword", dest="password")
# set the logger Level
args = parser.parse_args()
logger.setLevel(getattr(logging, args.logLevel))

# Display the namespace
logger.debug(args)

# Display for each properties (kind, artist, album) what is inside
for propriete in ["kind_name", "artist_name", "album_name"]:     
    logger.debug(propriete + " == " + str(getattr(args, propriete)))

# Display what is inside the differents variable for the namespace
logger.debug("duration : "+str(args.duration)+" (minutes)")
logger.debug("title : "+str(args.title))

# Starting main function
def main():
    logger.debug("Enter of main")
    logger.debug("Port : " + str(args.port))
    logger.debug("Host : " + args.host)
    logger.debug("DataBase : " + args.host)
    try:
        # Connect to an existing database
        conn = psycopg2.connect(database=args.database, user=args.user, password=args.password , host=args.host, port=str(args.port))
    except psycopg2.Error: 
        logger.critical("Error connection of Databse")
        sys.exit(0)    
        
    # Open a cursor to perform database operations
    cur = conn.cursor()
   
   # Open a cursor to perform database operations
    cur = conn.cursor()

    #Test if the duration is not empty    
    if args.duration == None:
        # Display a critical message because duration is not filled
        logger.critical("Enter a valid duration!")
        sys.exit(0) # Leaving program
        
    #typeOfPlaylist = True --> generate a xspf
    #typeOfPlaylist = False --> generate a m3u
    if args.m3u and args.xspf:
        # Display critical message because m3u and xspf are selected
        logger.critical("Select between '--xspf' and '--m3u'! Not both, they are mutualy exclusive.")
        sys.exit(0) # Leaving program
    # If only xspf has been selected
    elif args.xspf:
        # A playlist '.xspf' will be created
        typeOfPlaylist=True
        # Display a debug message about the type of the playlist
        logger.debug("xspf selected")
    # If only m3u has been selected
    elif args.m3u:
        # A playlist '.m3u' will be created
        typeOfPlaylist=False
        # Display a debug message about the type of the playlist
        logger.debug("m3u selected") 
    # If nothing is selected between xspf and m3u
    else:
        logger.critical("Select between '--xspf' and '--m3u'!")
        sys.exit(0)  # Leaving program
     
    # Transform duration (minutes) into duration (secondes)
    args.duration = args.duration*60 
    # If args.title isn't empty and for each title in args.title
    if args.title != None:
        for title in args.title:
            # Do a request with the title given
            request = cur.execute("SELECT title, duration,path FROM mickael_theo.track WHERE title  ~'"+ title+ "';")
            request = cur.fetchall()
            # If the request return 0 result
            if request == []:
                # Display an error message
                logger.error("No result found for "+title+" in the database.")
            # If the request return more than 0 results
            else:
                # Display the name of the time of the title
                logger.debug("Time of the song "+title+" : "+str(request[0][1]))   
                # Remove time of the song in the current duration                
                soustraction=int(request[0][1])
                # Do the calcul
                args.duration = args.duration - soustraction
            
    
    somme_Pourcentage = 0
    # For each properties (kind_name, artist_name and album_name), if the properties isn't empty
    # For each element in each properties
    proprietes = ["kind_name", "artist_name", "album_name"]
    for propriete in proprietes:
        if getattr(args, propriete) != None:
            for element in getattr(args, propriete):
                # Try if the element[1] (the duration) is string and if it's possible to convert in an integer
                try:
                    # Convert the string into an int
                    element[1] = int(element[1])
                    somme_Pourcentage = somme_Pourcentage + element[1]
                # If it's not an integer
                except ValueError:
                    # Display a critical message because it's not an integer
                    logger.critical("Enter a number, not a String ! Read the documentation for more information.")
                    sys.exit(0) # Leaving proram
    name = "Playlist : "
    # if kind_name isn't empty OR artist_name ins't empty Or album
    if args.kind_name != None or args.artist_name != None or args.album_name != None:
        # If sum is different 100 
        if somme_Pourcentage != 100:
            # Do the proportionality
            coefficientProportionnel = 100 / somme_Pourcentage
            for propriete in proprietes:
                if getattr(args, propriete) != None:
                    for element in getattr(args, propriete):
                        # Adjust different percentage and round the percentage
                        element[1] = element[1] * coefficientProportionnel
                        element[1] = round(element[1])
                        # Display every percentage                        
                        logger.debug("Percent "+element[0]+" : " + str(element[1]))
                        name += str(element[0]) +"_"+ str(element[1]) + " "

    # Display the duration in seconde (after conversion and removing duration from title(s))
    logger.debug("duration : "+str(args.duration)+" (seconde)")
    
    # Create an empty list
    playList = []
    
    # if no argument and a user enter duration
    if args.kind_name == None and args.title == None and args.album_name == None and args.artist_name == None:
            testargumentduration = 0
            logger.debug("No argurment")            
            request = cur.execute("SELECT title, duration, path, artist_name FROM mickael_theo.track;")
            request=cur.fetchall()
            # Shuffle the request                    
            shuffle(request)
            # For each tuple in request
            for tuple in request: 
                # If duration isn't further than 0.5% or the duration                
                if (testargumentduration + tuple[1] > args.duration-0.5/100*args.duration):
                    pass
                else:
                    
                    # Increase argumentation
                    testargumentduration = testargumentduration + tuple[1]
                    # Display a debug message with the tuple and the time
                    logger.debug("Title : " + tuple[0] + " Time : " + str(tuple[1]) +"\n")
                    # Add the song at the playlist
                    playList.append(tuple)            
                
    # For each properties
    for propriete in proprietes:        
        # If the property is not empty
        if getattr(args, propriete) != None:
            # For each property
            for element in getattr(args, propriete):
                # Do the request with the given property
                request = cur.execute("SELECT title, duration, path, artist_name FROM mickael_theo.track WHERE " + propriete + " ~'"+element[0]+ "';")
                request=cur.fetchall()
                # If the request return 0 result
                if request == []:
                    # Display an error message
                    logger.error("No result found for "+element[0])
                # If the request return more than 0 results
                else:
                    # Make the percentage
                    duration = args.duration*element[1]/100
                    testargumentduration = 0
                    logger.debug("Time after conversion : " + str(duration))
                    # Shuffle the request                    
                    shuffle(request)
                    # For each tuple in request
                    for tuple in request:     
                        # If duration isn't further than 0.5% or the duration
                        if (testargumentduration + tuple[1] > duration-0.5/100*duration):
                            pass
                        else:
                            # Increase argumentation
                            testargumentduration = testargumentduration + tuple[1]
                            # Display a debug message with the tuple and the time
                            logger.debug("Title : " + tuple[0] + " Time : " + str(tuple[1]) +"\n")
                            # Add the song at the playlist
                            playList.append(tuple)        
                    # Display two dedug messages
                    logger.debug("Element : " + str(element[0]) + " property : " + propriete)  
                    logger.debug("Time of "+str(element[0])+" : "+str(testargumentduration)+ "sec")
                    
                
    # If title ins't empty
    if args.title != None:
        name += str(args.title)
        # For each title
        for title in args.title:
            # Do the request with the given title
            request = cur.execute("SELECT title, duration, path, artist_name FROM mickael_theo.track WHERE title  ~'"+ title+ "';")
            request=cur.fetchall()  
            # If the request return 0 result
            if request == []:
                # Display an error message 
                logger.error("No result found for "+title+" in the database.")
            # If the request return more than 0 results
            else:
                # Display the tile and the request
                logger.debug("Element title : " + str(title))            
                logger.debug("request Title : " + str(request[0][1]))
                # Add the song at the playlist
                playList.append(request[0])
            
    # Display the whole playlist
    logger.debug("full playlist : "+str(playList))
    
    fullDuration=0
    time=0
    # For each title in the playlist
    for title in playList:
        # Convert the time into an integer
        time=int(title[1])
        # Calcul the time of the playlist
        fullDuration=fullDuration+time
    # Display the lenght of the playlist
    fullDuration = round(fullDuration/60)
    logger.info("Full time playlist is "+str(fullDuration)+ " minutes, "+str(fullDuration*60)+ " secondes" )
    

    # Close communication with the database    
    cur.close()
    conn.close()
    
    # If the playlist is empty
    if playList==[]:
        # Display a critical message because the playlist is empty
        logger.critical("No result founded! Playlist not created. Read the documentation for more information.")
        sys.exit(0)  # Leaving program
    
    #Shuffle the playlist if shuffle is true
    if args.shuffle:
        shuffle(playList)
        logger.info("Playlist shuffled.")
    else:
        pass
    logger.debug("Name of Playlist : "+ name) 
    if typeOfPlaylist==True:
        #For XSPF
        m3uAndXspfFile.makexspf(name, fullDuration, playList)
            
    else:
        #For M3U
        m3uAndXspfFile.makem3u(name, fullDuration, playList)
            
    
    logger.debug("Exiting main")

if __name__ == "__main__":
    logger.debug("Execution of module 'generator'")
    # Calling function 'main'
    main()
    logger.debug("Finish execution of module 'generator'")
else:
    logger.debug("Loading of module 'generator'")
