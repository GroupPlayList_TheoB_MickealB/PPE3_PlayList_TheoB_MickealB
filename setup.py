from setuptools import setup

setup(
	name="generator",
	version="1.0",
	author="Théo Bouchevreau and Mickeal Boutruche",
	author_email="theo.bouchevreau@bts-malraux.net, mickael.boutruche@bts-malraux.net",
	description="This program can generate playlist",
	long_description="This program can generate playlist with specifics arguments",
	license="GPLv3",
	url="https://framagit.org/GroupPlayList_TheoB_MickealB/PPE3_PlayList_TheoB_MickealB",
	install_requires=['psycopg2', 'lxml'],
	data_files=[('/usr/share/man/man1', ['doc/generator.1'])],
	packages=['generator'],
	entry_points={
		'console_scripts': [
			'generator = generator.generator:main'
		]
	}
)

