# -*- coding: utf-8 -*-
import logging
import argparse
import psycopg2
from random import shuffle
# Connect to an existing database
conn = psycopg2.connect(database="radio_libre", user="t.bouchevreau", password="P@ssword", host="bts.bts-malraux72.net", port="62543")

# Open a cursor to perform database operations
cur = conn.cursor()

formatter = logging.basicConfig(format='%(filename)s:%(levelname)s:%(message)s')
logger = logging.getLogger('generator')

parser = argparse.ArgumentParser()
parser.add_argument("-l, --log-level", type=str, help="log level", dest="logLevel", default="DEBUG", choices=["DEBUG", "INFO", "WARNING", "ERROR", "CRITICAL"])
parser.add_argument("--duration", type=int, help="enter duration", dest="duration")
parser.add_argument("--kind", help="enter kind (first argument is the name and second the percentage)", nargs=2, dest="kind_name", action="append")
parser.add_argument("--artist", type=str, help="enter artist (first argument is the name and second the percentage)", nargs=2, dest="artist_name", action="append")
parser.add_argument("--album", type=str, help="enter album (first argument is the name and second the percentage)", nargs=2, dest="album_name", action="append")
parser.add_argument("--title", type=str, help="enter title", dest="title", action="append")

args = parser.parse_args()
logger.setLevel(getattr(logging, args.logLevel))

logger.info("duration : "+str(args.duration))
logger.info("kind : "+str(args.kind_name))
logger.info("artist : "+str(args.artist_name))
logger.info("album : "+str(args.album_name))
logger.info("title : "+str(args.title))   

args.duration = args.duration*60 
if args.title != None:
    for title in args.title:
        request = cur.execute("SELECT title, duration,path FROM mickael_theo.track WHERE title  ~'"+ title+ "';")
        request=cur.fetchall()
        logger.info("request[0][1] : "+str(request[0][1]))   
        soustraction=int(request[0][1])
        logger.info((str(soustraction)))
        args.duration = args.duration - soustraction
        
somme_Pourcentage = 0
proprietes = ["kind_name", "artist_name", "album_name"]
for propriete in proprietes:
    if getattr(args, propriete) != None:
        for element in getattr(args, propriete):            
            element[1] = int(element[1])
            somme_Pourcentage = somme_Pourcentage + element[1]
        
if args.kind_name != None or args.artist_name != None or args.album_name != None:
    if somme_Pourcentage != 100:
        coefficientProportionnel = 100 / somme_Pourcentage
        for propriete in proprietes:
            if getattr(args, propriete) != None:
                for element in getattr(args, propriete):
                    element[1] = element[1] * coefficientProportionnel
                    element[1] = round(element[1],0)
                    logger.info("Test : " + str(element[1]))     
  

logger.info("duration : "+str(args.duration))
logger.info("kind : "+str(args.kind_name))
logger.info("artist : "+str(args.artist_name))
logger.info("album : "+str(args.album_name))
logger.info("title : "+str(args.title))

playList = []
for propriete in proprietes:
    if getattr(args, propriete) != None:
        for element in getattr(args, propriete):
            request = cur.execute("SELECT title, duration, path FROM mickael_theo.track WHERE " + propriete + " ~'"+element[0]+ "';")
            request=cur.fetchall()
            duration = args.duration*element[1]/100
            testargumentduration = 0
            logger.info("Time after conversion : " + str(duration))
            shuffle(request)
            for tuple in request:                                
                if (testargumentduration + tuple[1] > duration-2/100*duration):
                    pass
                else:
                    testargumentduration = testargumentduration + tuple[1]
                    print("Title : " + tuple[0] + " Temps : " + str(tuple[1]) +"\n")
                    playList.append(tuple[2])                            
            logger.info("Element : " + str(element[0]) + " propriete " + propriete)            
            

if args.title != None:
    for title in args.title:
        request = cur.execute("SELECT title, duration, path FROM mickael_theo.track WHERE title  ~'"+ title+ "';")
        request=cur.fetchall()            
        logger.info("Element title : " + str(title))            
        logger.info("request Title : " + str(request[0][1]))
        playList.append(request[0][2])
        
logger.debug(playList)
        

