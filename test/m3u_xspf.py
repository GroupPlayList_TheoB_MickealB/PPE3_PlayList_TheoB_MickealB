# -*- coding: utf-8 -*-

import psycopg2
from random import shuffle
from lxml import etree
import sys

import m3uAndXspfFile
# Connect to an existing database
conn = psycopg2.connect(database="radio_libre", user="t.bouchevreau", password="P@ssword", host="bts.bts-malraux72.net", port="62543")

# Open a cursor to perform database operations
cur = conn.cursor()

duration=30
timePlaylist = duration*60
timeUsed = 0
playList=[]
while timePlaylist > timeUsed+30:
    cur.execute("SELECT title, duration, path, artist_name FROM mickael_theo.track WHERE title ~ '.*é.*';")
    resultat=cur.fetchall()
    shuffle(resultat)
    timeUsed= timeUsed+resultat[1][1]
    playList.append(resultat[1])
    print (str(timeUsed))
name="Rock_14 Arist_Jean_25"
fullDuration=14

#m3uAndXspfFile.makem3u(name, fullDuration, playList)

#m3uAndXspfFile.makexspf(name, fullDuration, playList)